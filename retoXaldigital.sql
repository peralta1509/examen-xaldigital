create database PruebaSql;
use PruebaSql;

create table aerolineas(
id_aerolinea int not null,
nombre_aerolinea varchar(45),
PRIMARY KEY (id_aerolinea)
 );

create table aeropuertos(
id_aeropuerto int not null,
nombre_aeropuerto varchar(45),
PRIMARY KEY (id_aeropuerto)
 );

create table movimientos(
id_movimiento int not null,
descripcion varchar(45),
PRIMARY KEY (id_movimiento)
 );

create table vuelos(
id_aerolinea int not null,
id_aeropuerto int not null,
id_movimiento int not null,
dia date
 );

INSERT INTO aerolineas VALUES ('1','Volaris');
INSERT INTO aerolineas VALUES('2' , 'Aeromar' );
INSERT INTO aerolineas VALUES( '3 ', 'Interjet ');
INSERT INTO aerolineas VALUES( '4' ,'Aeromexico' );

INSERT INTO aeropuertos VALUES ('1','Benito Juarez');
INSERT INTO aeropuertos VALUES('2' , 'Guanajuato');
INSERT INTO aeropuertos VALUES( '3 ', 'La Paz');
INSERT INTO aeropuertos VALUES( '4' ,'Oaxaca');

INSERT INTO movimientos VALUES ('1','salida');
INSERT INTO movimientos VALUES('2' , 'llegada');

INSERT INTO vuelos VALUES('1','1','1','2021-05-02');
INSERT INTO vuelos VALUES('2','1','1','2021-05-02');
INSERT INTO vuelos VALUES('3','2','2','2021-05-02');
INSERT INTO vuelos VALUES('4','3','2','2021-05-02');
INSERT INTO vuelos VALUES('1','3','2','2021-05-02');
INSERT INTO vuelos VALUES('2','1','1','2021-05-02');
INSERT INTO vuelos VALUES('2','3','1','2021-05-04');
INSERT INTO vuelos VALUES('3','4','1','2021-05-04');
INSERT INTO vuelos VALUES('3','4','1','2021-05-04');


select *  from vuelos;

-- ¿Cuál es el nombre aeropuerto que ha tenido mayor movimiento durante el año?

SELECT
  p.nombre_aeropuerto, pt.id_aeropuerto, count(pt.id_aeropuerto) as total_movimientos
FROM
  aeropuertos p
LEFT JOIN vuelos pt
ON p.id_aeropuerto=pt.id_aeropuerto
GROUP BY id_aeropuerto
ORDER BY total_movimientos DESC;


-- ¿Cuál es el nombre aerolínea que ha realizado mayor número de vuelos durante el año?

SELECT
  p.nombre_aerolinea, pt.id_aerolinea, count(pt.id_aerolinea) as total_vuelos
FROM
  aerolineas p
LEFT JOIN vuelos pt
ON p.id_aerolinea=pt.id_aerolinea
GROUP BY id_aerolinea
ORDER BY total_vuelos DESC;


-- ¿En qué día se han tenido mayor número de vuelos?
SELECT dia, COUNT( dia ) AS total
FROM  vuelos
GROUP BY dia
ORDER BY total DESC;


-- ¿Cuáles son las aerolíneas que tienen mas de 2 vuelos por día?
SELECT id_aerolinea,dia, COUNT( id_aerolinea ) AS total
FROM  vuelos
GROUP BY id_aerolinea,dia
ORDER BY total;


DROP DATABASE PreubaSql;
