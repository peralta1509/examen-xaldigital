# examen Xaldigital
<html>
<head>
<style>
	.titulo {
		position: relative;
		font-family: Arial, Helvetica, sans-serif;
		font-size: 22px;
		color: #004481;
	}
	.fondoBody {background-color:#E7E9EB;}
	.contenedor_Div {
		position: relative;
		width: 420px;
		height: 450px;
		top: 8%;
		left: 35%;
		background: rgba(255,255,255, 1);
		border-radius: 1px;
		font-family: Arial, Helvetica, sans-serif;
		border: solid 1px #f0f0f0;
		text-align:center;
	}
	
	.subtitulo {
		position: relative;
		left: 10px;
		color: #909497;
		background: transparent;
		width: 350px;
		font-family: Arial, Helvetica, sans-serif;
		font-size: 15px;
		font-weight: 500;
		padding-left: 5px;
		text-align:center;
	}
	.indicaciones {
		position: relative;
		top: 4%;
		width: 60%;
		
		font-size: 15px;
		text-align: left;
		color: #2a86ca;
		text-decoration: none;
		background-color: transparent;
	}
		
	.boton {
		font-size: 1rem;
		background-color: #2874A6;
		border-color: transparent;
		border-radius: 5px;
		color: #fff;
		cursor: pointer;
	}
</style>
<script>
	
	function obtenerValores() {
	<!-- conexion a elace -->
		var URL = "https://api.stackexchange.com/2.2/search?order=desc&sort=activity&intitle=perl&site=stackoverflow"
		var respuestaJSON={};
		var req = new XMLHttpRequest();
		var tamObj=0;
		var is_answered="";
		var is_answeredTrue=0;
		var is_answeredFalse=0;
		req.open('GET', URL, false);
		req.send(null);
		if (req.status == 200)
			<!-- respuestaJSON = JSON.stringify(req.responseText); -->
			var respText= (req.responseText);
			respuestaJSON = JSON.parse(respText);
		try{
			tamObj = respuestaJSON.items.length;
		 }catch(e){
			alert('Error:'+e.message);
		}
		<!-- Obtiene el número de respuestas contestadas y no contestadas -->
		for(var i=0; i<tamObj; i++){
			is_answered = respuestaJSON.items[i].is_answered;
			
			if(is_answered==true){
				is_answeredTrue = is_answeredTrue + 1;
			}else{
				is_answeredFalse = is_answeredFalse + 1;
			}
			
		}
		
		<!-- se usa el .map para la busqueda de datos dentro de una matriz -->
		var resOwners = respuestaJSON.items.map(fnOwners);
		var resViewCount = respuestaJSON.items.map(fnViewCount);
		var resAnswerCount = respuestaJSON.items.map(fnAnswerCount);
		<!-- alert(Math.max(...resultado)); -->
		<!-- alert("Total de respuestas contestadas: "+is_answeredTrue+"<br /> Total de respuestas no contestadas: "+is_answeredFalse); -->
		
		<!-- manda a imprimir todos los resultados del punto 2 al 5 -->
		document.getElementById("examen").innerHTML ="<table class='subtitulo' style='top:20%' align='center'>"
															
															+"<tr class='titulo'>"
																+"<th>"
																	+"Resultados"
																+"<th>"
															+"</tr>"
															+"<tr class='indicaciones'>"
																+"<td>" 
																	+"<ul>"
																		 +"<li>Total de respuestas contestadas: "+is_answeredTrue+"</li>"
																		 +"<li> Total de respuestas no contestadas: "+is_answeredFalse+"</li>"
																		 +"<li> Owner con mayor respuesta: "+Math.max(...resOwners)+"</li>"
																		 +"<li> ViewCount con menor vistas: "+Math.min(...resViewCount)+"</li>"
																		 +"<li> Respuesta más vieja: "+Math.max(...resAnswerCount)+"</li>"
																		 +"<li> Respuesta más actual: "+Math.min(...resAnswerCount)+"</li>";
																	+"</ul>"
																+"</td>"
															+"</tr>"
														+"<table>";
																	 
		
		console.log("Total de respuestas contestadas: "+is_answeredTrue
													 +"<br /> Total de respuestas no contestadas: "+is_answeredFalse
													 +"<br /> Owner con mayor respuesta: "+Math.max(...resOwners)
													 +"<br /> ViewCount con menor vistas: "+Math.min(...resViewCount)
													 +"<br /> Respuesta más vieja: "+Math.max(...resAnswerCount)
													 +"<br /> Respuesta más actual: "+Math.min(...resAnswerCount));											 
		<!-- alert(respuestaJSON.items[0].tags[0]); -->
	}
	<!--  Obtiene todas las respuesta de los owners y los guarda en un arreglo -->
	function fnOwners(item){
		var resItem = [item.owner.reputation].join(" ");
		<!-- alert(resItem[0]); -->
		return resItem
	}
	<!-- Obtiene todas la respuestas de los números de vistas y los guarda en un arreglo -->
	function fnViewCount(item){
		var resItem = [item.view_count].join(" ");
		<!-- alert(resItem[0]); -->
		return resItem
	}
	<!-- Obtine todas las respuestas más viejas y más actuales y los guarda en un arreglo -->
	function fnAnswerCount(item){
		var resItem = [item.answer_count].join(" ");
		<!-- alert(resItem[0]); -->
		return resItem
	}
	
	function cancelar(){
		document.getElementById("examen").innerHTML = "<table class='subtitulo' style='top:20%' align='center'>"
															+"<tr>"
																+"<td>"
																	+"Hasta pronto gracias"
																+"</td>"
															+"</tr>"
														+"<table>";
														
	}
</script>
</head>
<body class="fondoBody">
	<div id="examen" width="100%" class="contenedor_Div">
		<table align="center">
			<tr>
				<th>
					<label class="titulo">Examen Xaldigital </label><br />
					<img src="./iconoPensando.png" width="75px" height="75px" id="imagenSeguridad" class="imagenSeguridad">
				</th>
			<tr>
			<tr>
				<td>
					<table>
						<tr>
							<td class="subtitulo">
								<label>
									La siguiente prueba demuestra conocimientos b&aacute;sicos sobre el manejo de objetos consumidos por una petici&oacute;n get
								</label>
							</td>
						</tr>
						<tr>
							<td class="indicaciones">
								<ul>
									<li> Conexion a enlace URL</li>
									<li> Obtener el total de respuestas contestadas </li>
									<li> Obtener el total de respuestas no contestadas </li>
									<li> Obtener el owner con mayor respuestas </li>
									<li> Obtener el view_count con menor vistas </li>
									<li> Obtener la respuesta m&aacute;s vieja </li>
									<li> Obtener la respuesta m&aacute;s actual </li>
								</ul>
							</td>
						</tr>
						<tr>
							<td>
								&nbsp;
							</td>
						</tr>
						<tr>
							<td align="center">
								<button id="btnCancelar" class="boton" title="Clic para cancelar" onclick="cancelar()"><span>Cancelar</span></button>
								<label>&nbsp;&nbsp;</label>
								<button id="btnAceptar" class="boton" title="Clic para mostrar los resultados de las indicaciones" onclick="obtenerValores()"><span>Aceptar</span></button>
							</td>
						</tr>
					</table>
				</td>
			</tr>
		</table>
	</div>
</body>
</html>
retos xaldigital
